-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: easyrez-sandra
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `datosfacturacioneasy`
--

DROP TABLE IF EXISTS `datosfacturacioneasy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datosfacturacioneasy` (
  `idDatosFacturacion` int NOT NULL AUTO_INCREMENT,
  `idTipoPersona` int DEFAULT NULL,
  `RFC` varchar(45) DEFAULT NULL,
  `RazonSocial` varchar(45) DEFAULT NULL,
  `idMetodoPago` int DEFAULT NULL,
  `idUsoCFDI` int DEFAULT NULL,
  `idRegimenFiscal` int DEFAULT NULL,
  `idDirecciónEasy` int DEFAULT NULL,
  `EsSucursal` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`idDatosFacturacion`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datosfacturacioneasy`
--

LOCK TABLES `datosfacturacioneasy` WRITE;
/*!40000 ALTER TABLE `datosfacturacioneasy` DISABLE KEYS */;
INSERT INTO `datosfacturacioneasy` VALUES (1,119,'XAXX010101000','Público en general',6,22,9,1,'No'),(2,119,'XEXX010101000','Público en general extranjero',6,22,9,1,'No'),(3,120,'ESO091210GK6‎','EZ SOLUTIONS S DE RL DE CV',6,22,13,1,'No'),(4,120,'RCH020621SP2','REACHCORE',6,22,13,1,'No'),(11,119,'RFCPrueba','Prueba Razon Social',6,22,9,1,'No'),(12,119,'RFCEasyPagina','PruebaEasyPagina',6,34,5,1,'Si');
/*!40000 ALTER TABLE `datosfacturacioneasy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-03 21:23:13
