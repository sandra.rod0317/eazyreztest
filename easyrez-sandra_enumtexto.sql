-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: easyrez-sandra
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `enumtexto`
--

DROP TABLE IF EXISTS `enumtexto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `enumtexto` (
  `idEnumTexto` int NOT NULL AUTO_INCREMENT,
  `Grupo` varchar(10) DEFAULT NULL,
  `Valor` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`idEnumTexto`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enumtexto`
--

LOCK TABLES `enumtexto` WRITE;
/*!40000 ALTER TABLE `enumtexto` DISABLE KEYS */;
INSERT INTO `enumtexto` VALUES (1,'2','No'),(2,'2','Opcional'),(3,'2','[0-9]{11}|[0-9]{18}'),(4,'2','[0-9]{10,11}|[0-9]{15,16}|[0-9]{18}|[A-Z0-9_]{10,50}'),(5,'2','[0-9]{10}|[0-9]{16}|[0-9]{18}'),(6,'2','[0-9]{16}'),(7,'2','[0-9]{10}'),(8,'2','[0-9]{15,16}'),(9,'2','Si'),(10,'3','Manual'),(11,'3','Automático'),(12,'9','RegimenFiscal'),(13,'9','Uso CFDI'),(14,'10','Fisica'),(15,'10','Moral'),(16,'10','Fisica-Moral'),(17,'11','IVA'),(18,'11','IEPS'),(19,'11','IVA-IEPS'),(20,'11','IVA-IEPS Opcional'),(21,'12','Descripcion'),(22,'12','Nota'),(23,'15','Traslado'),(24,'15','Retencion'),(25,'15','Traslado-Retencion'),(26,'17','Tasa'),(27,'17','Cuota'),(28,'17','Exento'),(29,'19','0'),(30,'19','74'),(31,'19','101'),(32,'19','115'),(33,'19','122'),(34,'19','123'),(35,'19','149'),(36,'19','150'),(37,'19','182'),(38,'19','188'),(39,'19','203'),(40,'19','205'),(41,'19','227');
/*!40000 ALTER TABLE `enumtexto` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-03 21:23:11
