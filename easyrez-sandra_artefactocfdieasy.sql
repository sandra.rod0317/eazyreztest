-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: easyrez-sandra
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artefactocfdieasy`
--

DROP TABLE IF EXISTS `artefactocfdieasy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artefactocfdieasy` (
  `idArtefactoCFDI` int NOT NULL AUTO_INCREMENT,
  `Tipo` varchar(45) DEFAULT NULL,
  `Clave` varchar(45) DEFAULT NULL,
  `Descripción` varchar(250) DEFAULT NULL,
  `AplicacionTPersona` int DEFAULT NULL,
  PRIMARY KEY (`idArtefactoCFDI`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artefactocfdieasy`
--

LOCK TABLES `artefactocfdieasy` WRITE;
/*!40000 ALTER TABLE `artefactocfdieasy` DISABLE KEYS */;
INSERT INTO `artefactocfdieasy` VALUES (1,'12','601','General de Ley Personas Morales',15),(2,'12','603','Personas Morales con Fines no Lucrativos',15),(3,'12','605','Sueldos y Salarios e Ingresos Asimilados a Salarios',14),(4,'12','606','Arrendamiento',14),(5,'12','607','Régimen de Enajenación o Adquisición de Bienes',14),(6,'12','608','Demás ingresos',14),(7,'12','610','Residentes en el Extranjero sin Establecimiento Permanente en México',16),(8,'12','611','Ingresos por Dividendos (socios y accionistas)',14),(9,'12','612','Personas Físicas con Actividades Empresariales y Profesionales',14),(10,'12','614','Ingresos por intereses',14),(11,'12','615','Régimen de los ingresos por obtención de premios',14),(12,'12','616','Sin obligaciones fiscales',14),(13,'12','620','Sociedades Cooperativas de Producción que optan por diferir sus ingresos',15),(14,'12','621','Incorporación Fiscal',14),(15,'12','622','Actividades Agrícolas, Ganaderas, Silvícolas y Pesqueras',15),(16,'12','623','Opcional para Grupos de Sociedades',15),(17,'12','624','Coordinados',15),(18,'12','625','Régimen de las Actividades Empresariales con ingresos a través de Plataformas Tecnológicas',14),(19,'12','626','Régimen Simplificado de Confianza',16),(20,'13','G01','Adquisición de mercancías.',16),(21,'13','G02','Devoluciones, descuentos o bonificaciones.',16),(22,'13','G03','Gastos en general.',16),(23,'13','I01','Construcciones.',16),(24,'13','I02','Mobiliario y equipo de oficina por inversiones.',16),(25,'13','I03','Equipo de transporte.',16),(26,'13','I04','Equipo de computo y accesorios.',16),(27,'13','I05','Dados, troqueles, moldes, matrices y herramental.',16),(28,'13','I06','Comunicaciones telefónicas.',16),(29,'13','I07','Comunicaciones satelitales.',16),(30,'13','I08','Otra maquinaria y equipo.',16),(31,'13','D01','Honorarios médicos, dentales y gastos hospitalarios.',14),(32,'13','D02','Gastos médicos por incapacidad o discapacidad.',14),(33,'13','D03','Gastos funerales.',14),(34,'13','D04','Donativos.',14),(35,'13','D05','Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).',14),(36,'13','D06','Aportaciones voluntarias al SAR.',14),(37,'13','D07','Primas por seguros de gastos médicos.',14),(38,'13','D08','Gastos de transportación escolar obligatoria.',14),(39,'13','D09','Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.',14),(40,'13','D10','Pagos por servicios educativos (colegiaturas).',14),(41,'13','S01','Sin efectos fiscales.  ',16),(42,'13','CP01','Pagos',16),(43,'13','CN01','Nómina',14);
/*!40000 ALTER TABLE `artefactocfdieasy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-03 21:23:06
