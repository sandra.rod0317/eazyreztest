-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: localhost    Database: easyrez-sandra
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clasificacioneasy`
--

DROP TABLE IF EXISTS `clasificacioneasy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clasificacioneasy` (
  `idClasificacion` int NOT NULL AUTO_INCREMENT,
  `Grupo` varchar(10) DEFAULT NULL,
  `Codigo` varchar(10) DEFAULT NULL,
  `Nombre` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idClasificacion`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clasificacioneasy`
--

LOCK TABLES `clasificacioneasy` WRITE;
/*!40000 ALTER TABLE `clasificacioneasy` DISABLE KEYS */;
INSERT INTO `clasificacioneasy` VALUES (1,'1','1','Efectivo'),(2,'1','2','Cheque Nominativo'),(3,'1','3','Transferencia electrónica de fondos'),(4,'1','4','Tarjeta de crédito'),(5,'1','5','Monedero electrónico'),(6,'1','6','Dinero electrónico'),(7,'1','8','Vales de despensa'),(8,'1','12','Dación en pago'),(9,'1','13','Pago por subrogación'),(10,'1','14','Pago por consignación'),(11,'1','15','Condonación'),(12,'1','17','Compensación'),(13,'1','23','Novación'),(14,'1','24','Confusión'),(15,'1','25','Remisión de deuda'),(16,'1','26','Prescripción o caducidad'),(17,'1','27','A satisfacción del acreedor'),(18,'1','28','Tarjeta de débito'),(19,'1','29','Tarjeta de servicios'),(20,'1','30','Aplicación de anticipos'),(21,'1','31','Intermediario pagos'),(22,'1','99','Por definir'),(23,'4','I','Ingreso'),(24,'4','E','Egreso'),(25,'4','T','Traslado'),(26,'4','N','Nómina'),(27,'4','P','Pago'),(28,'5','1','No Aplica'),(29,'5','2','Definitva'),(30,'5','3','Temporal'),(31,'6','PUE','Pago en una sola exhibición'),(32,'6','PPD','Pago en parcialidades o diferido'),(33,'7','1','Diario'),(34,'7','2','Semanal'),(35,'7','3','Quincenal'),(36,'7','4','Mensual'),(37,'7','5','Bimestral'),(38,'8','1','Nota de crédito de los documentos relacionados'),(39,'8','2','Nota de débito de los documentos relacionados'),(40,'8','3','Devolución de mercancía sobre facturas o traslados previos'),(41,'8','4','Sustitución de los CFDI previos'),(42,'8','5','Traslados de mercancías facturados previamente'),(43,'8','6','Factura generada por los traslados previos'),(44,'8','7','CFDI por aplicación de anticipo'),(45,'13','64','Libra por pulgada cuadrada, calibre'),(46,'13','66','Oersted'),(47,'13','76','Gauss'),(48,'13','78','Kilogauss'),(49,'13','84','Kilopound-force por pulgada cuadrada'),(50,'14','1','No objeto de impuesto.'),(51,'14','2','Sí objeto de impuesto.'),(52,'14','3','Sí objeto del impuesto y no obligado al desglose.'),(53,'16','1','ISR'),(54,'16','2','IVA'),(55,'16','3','IEPS'),(56,'18','1','ACAPULCO, ACAPULCO DE JUAREZ, GUERRERO.'),(57,'18','2','AGUA PRIETA, AGUA PRIETA, SONORA.'),(58,'18','5','SUBTENIENTE LOPEZ, SUBTENIENTE LOPEZ, QUINTANA ROO.'),(59,'18','6','CIUDAD DEL CARMEN, CIUDAD DEL CARMEN, CAMPECHE.'),(60,'18','7','CIUDAD JUAREZ, CIUDAD JUAREZ, CHIHUAHUA.'),(61,'18','8','COATZACOALCOS, COATZACOALCOS, VERACRUZ.'),(62,'18','11','ENSENADA, ENSENADA, BAJA CALIFORNIA.'),(63,'18','12','GUAYMAS, GUAYMAS, SONORA.'),(64,'18','14','LA PAZ, LA PAZ, BAJA CALIFORNIA SUR.'),(65,'18','16','MANZANILLO, MANZANILLO, COLIMA.'),(66,'18','17','MATAMOROS, MATAMOROS, TAMAULIPAS.'),(67,'18','18','MAZATLAN, MAZATLAN, SINALOA.'),(68,'18','19','MEXICALI, MEXICALI, BAJA CALIFORNIA.'),(69,'18','20','MEXICO, DISTRITO FEDERAL.'),(70,'18','22','NACO, NACO, SONORA.'),(71,'18','23','NOGALES, NOGALES, SONORA.'),(72,'18','24','NUEVO LAREDO, NUEVO LAREDO, TAMAULIPAS.'),(73,'18','25','OJINAGA, OJINAGA, CHIHUAHUA.'),(74,'18','26','PUERTO PALOMAS, PUERTO PALOMAS, CHIHUAHUA.'),(75,'18','27','PIEDRAS NEGRAS, PIEDRAS NEGRAS, COAHUILA.'),(76,'18','28','PROGRESO, PROGRESO, YUCATAN.'),(77,'18','30','CIUDAD REYNOSA, CIUDAD REYNOSA, TAMAULIPAS.'),(78,'18','31','SALINA CRUZ, SALINA CRUZ, OAXACA.'),(79,'18','33','SAN LUIS RIO COLORADO, SAN LUIS RIO COLORADO, SONORA.'),(80,'18','34','CIUDAD MIGUEL ALEMAN, CIUDAD MIGUEL ALEMAN, TAMAULIPAS.'),(81,'18','37','CIUDAD HIDALGO, CIUDAD HIDALGO, CHIAPAS.'),(82,'18','38','TAMPICO, TAMPICO, TAMAULIPAS.'),(83,'18','39','TECATE, TECATE, BAJA CALIFORNIA.'),(84,'18','40','TIJUANA, TIJUANA, BAJA CALIFORNIA.'),(85,'18','42','TUXPAN, TUXPAN DE RODRIGUEZ CANO, VERACRUZ.'),(86,'18','43','VERACRUZ, VERACRUZ, VERACRUZ.'),(87,'18','44','CIUDAD ACUÑA, CIUDAD ACUÑA, COAHUILA.'),(88,'18','46','TORREON, TORREON, COAHUILA.'),(89,'18','47','AEROPUERTO INTERNACIONAL DE LA CIUDAD DE MEXICO,'),(90,'18','48','GUADALAJARA, TLACOMULCO DE ZUÑIGA, JALISCO.'),(91,'18','50','SONOYTA, SONOYTA, SONORA.'),(92,'18','51','LAZARO CARDENAS, LAZARO CARDENAS, MICHOACAN.'),(93,'18','52','MONTERREY, GENERAL MARIANO ESCOBEDO, NUEVO LEON.'),(94,'18','53','CANCUN, CANCUN, QUINTANA ROO.'),(95,'18','64','QUERÉTARO, EL MARQUÉS Y COLON, QUERÉTARO.'),(96,'18','65','TOLUCA, TOLUCA, ESTADO DE MEXICO.'),(97,'18','67','CHIHUAHUA, CHIHUAHUA, CHIHUAHUA.'),(98,'18','73','AGUASCALIENTES, AGUASCALIENTES, AGUASCALIENTES.'),(99,'18','75','PUEBLA, HEROICA PUEBLA DE ZARAGOZA, PUEBLA.'),(100,'18','80','COLOMBIA, COLOMBIA, NUEVO LEON.'),(101,'18','81','ALTAMIRA, ALTAMIRA, TAMAULIPAS.'),(102,'18','82','CIUDAD CAMARGO, CIUDAD CAMARGO, TAMAULIPAS.'),(103,'18','83','DOS BOCAS, PARAISO, TABASCO.'),(104,'18','84','GUANAJUATO, SILAO, GUANAJUATO.'),(105,'22','1','Enero'),(106,'22','2','Febrero'),(107,'22','3','Marzo'),(108,'22','4','Abril'),(109,'22','5','Mayo'),(110,'22','6','Junio'),(111,'22','7','Julio'),(112,'22','8','Agosto'),(113,'22','9','Septiembre'),(114,'22','10','Octubre'),(115,'22','11','Noviembre'),(116,'22','12','Diciembre'),(117,'30','C','Cliente'),(118,'30','E','Emisor'),(119,'31','PF','Persona Física'),(120,'31','PM','Persona Moral'),(121,'32','F','Facturación'),(122,'37','1','Comprobante emitido con errores con relación'),(123,'37','2','Comprobante emitido con errores sin relación'),(124,'37','3','No se llevó acabo la operación'),(125,'37','4','Operación nominativa relacionada en la factura'),(126,'30','PAC','Proveedor PAC'),(127,'13','H87','Pieza');
/*!40000 ALTER TABLE `clasificacioneasy` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-03 21:23:10
